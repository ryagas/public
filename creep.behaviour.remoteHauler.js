const mod = new Creep.Behaviour('remoteHauler');
module.exports = mod;
mod.nextAction = function (creep) {
	const flag = creep.data.destiny && Game.flags[creep.data.destiny.targetName];
	if (!flag) {
		return Creep.action.recycling.assign(creep);
	}

	// at home
	if (creep.pos.roomName == creep.data.homeRoom) {
		// carrier filled
		if (creep.sum > 0) {
			let deposit = []; // deposit energy in...

			// For deposit materials, prefer storage
			if (creep.room.storage) {
				deposit.push(creep.room.storage);
			}
			// terminal as backup
			if (creep.room.terminal) {
				deposit.push(creep.room.terminal);
			}

			// Choose the closest
			if (deposit.length > 0) {
				let target = creep.pos.findClosestByRange(deposit);
				if (this.assignAction(creep, 'storing', target)) return;
			}
			// no deposit available
			if (this.assignAction(creep, 'dropping')) return;
			else {
				const drop = r => { if (creep.carry[r] > 0) creep.drop(r); };
				_.forEach(Object.keys(creep.carry), drop);
				return this.assignAction(creep, 'idle');
			}
		}
		// empty
		// travelling
		if (this.gotoTargetRoom(creep)) {
			return;
		}
	}
	// at target room
	else if (creep.data.destiny.room == creep.pos.roomName) {
		if (creep.ticksToLive && creep.ticksToLive < (CREEP_LIFE_TIME * 0.3)) {
			return this.goHome(creep);
		}
		let mother = Game.spawns[creep.data.motherSpawn];
		if (creep.ticksToLive && creep.ticksToLive < (CREEP_LIFE_TIME * 0.1)) {
			this.assignAction(creep, Creep.action.recycling, mother);
		}
		if (creep.sum / creep.carryCapacity > REMOTE_HAULER.MIN_LOAD) {
			this.goHome(creep);
			return;
		}

		// Check if this is a deposit flag
		if (flag.memory.deposit) {
			// Look for dropped resources near deposit
			const deposit = Game.getObjectById(flag.memory.depositId);
			if (deposit) {
				const droppedResources = deposit.pos.findInRange(FIND_DROPPED_RESOURCES, 3);
				if (droppedResources.length > 0) {
					if (this.assignAction(creep, 'picking', droppedResources[0])) return;
				}
			}
		} else {
			// Regular mining behavior
			if (this.assignAction(creep, 'uncharging')) return;
			if (this.assignAction(creep, 'picking')) return;
		}

		// wait near source/deposit
		if (creep.sum === 0) {
			let target;
			if (flag.memory.deposit) {
				target = Game.getObjectById(flag.memory.depositId);
			} else {
				target = creep.pos.findClosestByRange(creep.room.sources);
			}
			if (target && creep.pos.getRangeTo(target) > 3) {
				creep.data.travelRange = 3;
				return this.assignAction(creep, 'travelling', target);
			}
		}
		return this.assignAction(creep, 'idle');
	}
	// somewhere else
	else {
		let ret = false;
		if (creep.sum / creep.carryCapacity > REMOTE_HAULER.MIN_LOAD)
			ret = this.goHome(creep);
		else
			ret = this.gotoTargetRoom(creep);
		if (ret) {
			return;
		}
	}

	// fallback - recycle self
	let mother = Game.spawns[creep.data.motherSpawn];
	if (mother) {
		this.assignAction(creep, Creep.action.recycling, mother);
	}
};

mod.gotoTargetRoom = function (creep) {
	const targetFlag = creep.data.destiny ? Game.flags[creep.data.destiny.targetName] : null;
	if (targetFlag) return Creep.action.travelling.assignRoom(creep, targetFlag.pos.roomName);
};

mod.goHome = function (creep) {
	return Creep.action.travelling.assignRoom(creep, creep.data.homeRoom);
};

mod.strategies.picking = {
	name: `picking-${mod.name}`,
	energyOnly: false
};