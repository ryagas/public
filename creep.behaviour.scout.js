// creep.behaviour.scout.js
const mod = new Creep.Behaviour('scout');
module.exports = mod;

// scout-specific behaviour
mod.actions = function (creep) {
	let priority = [
		Creep.action.scouting,
		Creep.action.recycling
	];
	return priority;
};

mod.run = function (creep) {
	// ensure valid state
	if (!creep.action) {
		this.assignAction(creep, 'scouting');
	}

	// Do nothing if we're still spawning
	if (creep.spawning) return;

	// If we have an action, execute it
	if (creep.action) {
		creep.action.step(creep);
		return;
	}
};