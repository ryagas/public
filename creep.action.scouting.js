// creep.action.scouting.js
const action = new Creep.Action('scouting');
module.exports = action;

action.targetRange = 1;
action.reachedRange = 1;

action.isValidTarget = function (target, creep) {
	if (!target) return false;
	if (!creep.data.destiny) return false;
	return true;
};

action.newTarget = function (creep) {
	if (creep.data.destiny && creep.data.destiny.roomTarget) {
		return new RoomPosition(25, 25, creep.data.destiny.roomTarget);
	}
	return null;
};

action.step = function (creep) {
	if (creep.data.destiny && creep.data.destiny.roomTarget) {
		// If we're in the target room
		if (creep.room.name === creep.data.destiny.roomTarget) {
			// Create the mining flag if not already there
			if (creep.data.destiny.flagTarget) {
				const flagPos = new RoomPosition(
					creep.data.destiny.flagTarget.x,
					creep.data.destiny.flagTarget.y,
					creep.room.name
				);

				const flag = Game.flags[creep.data.destiny.flagTarget.flagName];
				if (!flag) {
					flagPos.createFlag(
						creep.data.destiny.flagTarget.flagName,
						COLOR_GREEN,
						COLOR_BROWN
					);
				} else if (flag.memory.deposit && flag.memory.depositId) {
					// Only recycle once the flag is properly set up with deposit info
					delete creep.data.destiny.flagTarget;
					Creep.action.recycling.assign(creep);
					return;
				}

				// Stay at the flag position to provide vision
				if (creep.pos.getRangeTo(flagPos) > 0) {
					creep.moveTo(flagPos);
				}
			}
		} else if (creep.target) {
			// If not in target room, keep moving
			creep.travelTo(creep.target, { allowHostile: true });
		}
	}
};