const mod = new Creep.Behaviour('remoteMiner');
module.exports = mod;
const super_run = mod.run;

mod.run = function (creep) {
	// Avoid before doing anything else
	if (Creep.action.avoiding.run(creep)) {
		return;
	}

	// Get the target flag
	const flag = Game.flags[creep.data.destiny.targetName];

	if (!flag) {
		if (!creep.action || creep.action.name !== 'recycling') {
			Creep.action.recycling.assign(creep);
		}
		if (creep.action && creep.action.name == 'recycling') {
			creep.action.step(creep);
		}
		return;
	}

	// If not in the target room, move to it
	if (creep.room.name !== flag.pos.roomName) {
		creep.moveTo(flag, { visualizePathStyle: { stroke: '#ffaa00' } });
		return;
	}

	// Once in the target room
	if (flag.memory.deposit) {
		this.handleDeposit(creep, flag);
	} else {
		super_run.call(this, creep);
	}
};

mod.handleDeposit = function (creep, flag) {
	const deposit = Game.getObjectById(flag.memory.depositId);
	if (!deposit) {
		if (!creep.action || creep.action.name !== 'recycling') {
			Creep.action.recycling.assign(creep);
		}
		if (creep.action && creep.action.name == 'recycling') {
			creep.action.step(creep);
		}
		return;
	}

	// Check if we should remove the flag based on cooldown vs TTL
	if (deposit.lastCooldown == 100) {
		if (global.DEBUG && global.TRACE) {
			trace('RemoteMiner', {
				method: 'handleDeposit',
				creepName: creep.name,
				depositId: deposit.id,
				lastCooldown: deposit.lastCooldown,
				creepTTL: creep.ticksToLive,
				flagName: flag.name,
				pos: deposit.pos
			});
		}
		flag.remove();
		if (!creep.action || creep.action.name !== 'recycling') {
			Creep.action.recycling.assign(creep);
		}
		if (creep.action && creep.action.name == 'recycling') {
			creep.action.step(creep);
		}
		return;
	}

	// Move to deposit if not adjacent
	if (creep.pos.getRangeTo(deposit) > 1) {
		creep.moveTo(deposit, { visualizePathStyle: { stroke: '#ffaa00' } });
		return;
	}

	// Start harvesting if we have empty carry capacity
	if (creep.store.getFreeCapacity() > 0) {
		creep.harvest(deposit);
	} else {
		// Drop resources when full
		for (const resourceType in creep.store) {
			creep.drop(resourceType);
		}
	}
};

mod.actions = function (creep) {
	return Creep.behaviour.miner.actions.call(this, creep);
};

mod.getEnergy = function (creep) {
	return Creep.behaviour.miner.getEnergy.call(this, creep);
};

mod.maintain = function (creep) {
	return Creep.behaviour.miner.maintain.call(this, creep);
};