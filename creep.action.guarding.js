let action = new Creep.Action('guarding');
module.exports = action;

// Add isValidAction check that invalidates guarding when hostiles present
action.isValidAction = function (creep) {
	// Guard should be invalid if there are hostiles
	return creep.room.hostiles.length === 0;
};

action.isAddableAction = function () { return true; };
action.isAddableTarget = function () { return true; };
action.reachedRange = 0;
action.newTarget = function (creep) {
	var flag;
	if (creep.data.destiny) flag = Game.flags[creep.data.destiny.flagName];
	if (!flag) {
		flag = FlagDir.find(FLAG_COLOR.defense, creep.pos, false, FlagDir.rangeMod, {
			rangeModPerCrowd: 400
			//rangeModByType: creep.data.creepType
		});
	}

	if (creep.action && creep.action.name == 'guarding' && creep.flag)
		return creep.flag;
	if (flag) Population.registerCreepFlag(creep, flag);
	return flag;
};
action.work = function (creep) {
	if (creep.data.flagName)
		return OK;
	else return ERR_INVALID_ARGS;
};